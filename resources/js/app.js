import "./bootstrap";

import { createApp } from "vue/dist/vue.esm-bundler";
import Socket from "./components/socket.vue";

const app = createApp({});

app.component("socket", Socket);

app.mount("#app");
